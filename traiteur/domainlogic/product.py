class ProductDomainLogic:
    _instance = None

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()

        return cls._instance

    def validate_product(self, product):
        reasons = []
        required_fields = [
            'title',
            'vendor_id',
            'vendor_product_id',
            'url',
            'price',
        ]

        for field in required_fields:
            if not product.get(field):
                reasons.append('Product is missing {} field'.format(field))

        if not reasons:
            return { 'result': True, 'reasons': '' }

        return { 'result': False, 'reasons': reasons }
