from datetime import datetime

from dataclasses import dataclass
import scrapy

@dataclass
class TraiteurItem(scrapy.Item):
    title: str
    vendor_id: str
    vendor_product_id: str
    price: float
    url: str
    image: str
    rating: float
    reviews_count: int
    created_at: datetime
    updated_at: datetime
