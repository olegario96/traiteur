# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html

from dotenv import load_dotenv
from scrapy import signals

from .mongodb.client import MongoClient


class TraiteurSpiderMiddleware:
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        spider = cls()
        crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
        return spider

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, or item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Request or item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        load_dotenv()
        spider.logger.info('Environment vairables have been loaded')
        spider.logger.info('Spider opened: {}'.format(spider.name))
        spider.mongo_client = MongoClient.instance()

    def spider_closed(self, spider):
        spider.logger.info('Spider closed: {}'.format(spider.name))
        spider.mongo_client.close_connection()
        spider.logger.info('Connection with MongoDB has been closed')
