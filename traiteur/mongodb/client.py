from pymongo import MongoClient as MongoDbClient

from os import environ

class MongoClient:
    _instance = None

    def __init__(self):
        user = environ.get('MONGO_INITDB_ROOT_USERNAME')
        password = environ.get('MONGO_INITDB_ROOT_PASSWORD')
        host = environ.get('MONGO_HOST')
        port = environ.get('MONGO_PORT')
        database = environ.get('MONGO_INITDB_DATABASE')
        url = 'mongodb://{}:{}@{}:{}/{}'.format(user, password, host, port, database)
        self.client = MongoDbClient(url)
        self.database = self.client[database]

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()

        return cls._instance

    def upsert_product(self, product):
        collection = self.database.products
        query = { 'vendor_id': product.get('vendor_id'), 'vendor_product_id': product.get('vendor_product_id') }
        persisted_product = collection.find_one(query)
        if persisted_product is None:
            return collection.insert_one(product).inserted_id

        query = { '_id': persisted_product.get('_id') }
        target_values = {
            'price': product.get('price'),
            'rating': product.get('rating'),
            'reviews_count': product.get('reviews_count'),
            'updated_at': product.get('updated_at'),
        }

        collection.update_one(query, { '$set': target_values }, upsert=True)
        return persisted_product.get('_id')

    def close_connection(self):
        self.client.close()
