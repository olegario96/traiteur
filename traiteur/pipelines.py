from scrapy.exceptions import DropItem

from .domainlogic import ProductDomainLogic

class TraiteurPipeline:
    def __init__(self):
        self.product_domain_logic = ProductDomainLogic.instance()

    def process_item(self, item, spider):
        product_validation = self.product_domain_logic.validate_product(item)
        if not product_validation.get('result'):
            raise DropItem('Item does not match domain logic requirements. {}'.format(product_validation.get('reasons')))

        spider.mongo_client.upsert_product(item)
        return item
