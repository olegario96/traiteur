import scrapy

QUERY_CATEGORIES_LINKS = "//li[@class='menu-dropdown-item']/a[@class='menu-dropdown-link']/@href"
QUERY_PRODUCT_CARD = "//div[@data-store='1']"
QUERY_NEXT_PAGE = "//a[@rel='next']/@href"
QUERY_PRODUCT_TITLE = "//p[@class='product-box-title']/text()"
QUERY_VENDOR_PRODUCT_ID = "//div[@class=' product-box-image']/@id"
QUERY_PRICE = "//span[@class='product-box-price-from']/text()"
QUERY_PRICE_FALLBACK = "//span[@class='product-box-price-to']/text()"
QUERY_URL = "//div[@class=' product-box-image']/a/@href"
QUERY_IMAGE = "//img/@src"
QUERY_RATING = "//span[contains(@class, 'rating-value')]/@style"
QUERY_REVIEWS_COUNT = "//span[@class='ratings-count']/text()"
class DafitiSpider(scrapy.Spider):
    name = 'dafiti'
    allowed_domains = [
        'dafiti.com.br',
    ]

    def start_requests(self):
        start_url = 'https://dafiti.com.br'
        yield scrapy.Request(start_url, self.parse_landing_page)

    def parse_landing_page(self, response):
        print(response)
