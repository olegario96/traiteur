def parse_price(price):
    target_price = price.split(' ')[-1]
    price_parsed = target_price.replace('.', '').replace(',', '.')
    return float(price_parsed)
